# Devops Home Assignment
## Setup
Hi there, in order to setup this server locally clone the repo -> install node.js -> run ```npm install``` -> run ```npm start``` and wait for the "server started!" message.

Then you can test the server by sending HTTP Get request to http://localhost:3000 and get the following list:
```javascript
[
    {
        "id": 1,
        "name": "Roei Rom"
    }
]
```

explore the server :) Good Luck!